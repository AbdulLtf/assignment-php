<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataDetail extends Model
{
    protected $table = 'order_item_detail';
    public $timestamps = false;
}
