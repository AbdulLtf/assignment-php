<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Data;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *   path="/data",
     *   summary="Get Testing",
     *   operationId="testing",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error"),
     *		@SWG\Parameter(
     *          name="data",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     * )
     *
     */

    public function index(Request $request)
    {
        echo $request->mytest;

        $data = Data::all();
        return view('data/index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

        $request->validate([
            'orderId' => 'required',
            'invoceNumber' => 'required',
            'orderName' => 'required',
            'orderDescription' => 'required'
        ]);

        $data = new Data;
        $data->orderId = $request->orderId;
        $data->invoceNumber = $request->invoceNumber;
        $data->orderName = $request->orderName;
        $data->orderDescription = $request->orderDescription;
        $data->createdBy = 'sherlock';
        $data->createdDate = '2020-06-26T06:56:43';
        $data->modifiedBy = 'sherlock';
        $data->modifiedDate = '2020-06-26T06:56:43';
        $data->save();
        return redirect('/data')->with('Status', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        // dd($data);
        $detail = Data::join('order_item_detail', 'order_item_detail.orderDetailId', 'order_item.orderId')->select('order_item_detail.*')->where('order_item_detail.orderDetailId', '=', $data->orderId)->get();
        return view('data/show', ['data' => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Data $data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Data $data)
    {
        //
    }
}
