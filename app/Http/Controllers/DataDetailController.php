<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\DataDetail;

class DataDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dataDetail/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

        $request->validate([
            'orderDetailId' => 'required',
            'orderDetailItem' => 'required',
            'orderDetailItemQuantity' => 'required',
            'orderDetailItemPrice' => 'required',
            'orderDetailMerchant' => 'required'
        ]);

        $data = new DataDetail;
        $data->orderDetailId = $request->orderDetailId;
        $data->orderDetailItem = $request->orderDetailItem;
        $data->orderDetailItemQuantity = $request->orderDetailItemQuantity;
        $data->orderDetailItemPrice = $request->orderDetailItemPrice;
        $data->orderDetailMerchant = $request->orderDetailMerchant;

        $data->save();
        return redirect('/data')->with('Status', 'Data Detail Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
