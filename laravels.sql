-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jul 2020 pada 04.01
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravels`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_07_16_153448_create_order_item_table', 2),
(4, '2020_07_16_153510_create_order_item_detail_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `orderId` varchar(100) NOT NULL,
  `invoiceNumber` varchar(10) NOT NULL,
  `orderName` varchar(30) NOT NULL,
  `orderDescription` varchar(100) NOT NULL,
  `createdBy` varchar(50) NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` varchar(50) NOT NULL,
  `modifiedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `orderId`, `invoiceNumber`, `orderName`, `orderDescription`, `createdBy`, `createdDate`, `modifiedBy`, `modifiedDate`) VALUES
(1, 'd52a0843-2b2e-4ba8-8f59-ce97739eedd4', 'INV/001', 'Order 001', 'Order 001', 'sherlock', '2020-06-26 06:56:43', 'sherlock', '2020-06-26 06:56:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail`
--

CREATE TABLE `order_detail` (
  `id` bigint(20) NOT NULL,
  `orderDetailId` varchar(100) NOT NULL,
  `orderDetailItem` varchar(100) NOT NULL,
  `orderDetailItemQuantity` int(11) NOT NULL,
  `orderDetailItemPrice` int(11) NOT NULL,
  `orderDetailMerchant` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `order_detail`
--

INSERT INTO `order_detail` (`id`, `orderDetailId`, `orderDetailItem`, `orderDetailItemQuantity`, `orderDetailItemPrice`, `orderDetailMerchant`) VALUES
(1, 'd52a0843-2b2e-4ba8-8f59-ce97739eedd4', 'iPhone', 1, 14000000, 'iBox'),
(2, 'd52a0843-2b2e-4ba8-8f59-ce97739eedd4', 'Charger WiFi Universel', 1, 1500000, 'Erafone');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_item`
--

CREATE TABLE `order_item` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoceNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdDate` datetime NOT NULL,
  `modifiedBy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ModifiedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `order_item`
--

INSERT INTO `order_item` (`id`, `orderId`, `invoceNumber`, `orderName`, `orderDescription`, `createdBy`, `createdDate`, `modifiedBy`, `ModifiedDate`) VALUES
(1, 'd52a0843-2b2e-4ba8-8f59-ce97739eedd4', 'INV/001', 'Order 001', 'Order 001', 'sherlock', '2020-06-26 06:56:43', 'sherlock', '2020-06-26 06:56:43'),
(2, 'f52a0843-2b2e-4ba8-8f59-ce97739eedd3', 'INV/002', 'Order 002', 'Order 002', 'sherlock', '2020-06-26 06:56:43', 'sherlock', '2020-06-26 06:56:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_item_detail`
--

CREATE TABLE `order_item_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `orderDetailId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderDetailItem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderDetailItemQuantity` int(11) NOT NULL,
  `orderDetailItemPrice` int(11) NOT NULL,
  `orderDetailMerchant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `order_item_detail`
--

INSERT INTO `order_item_detail` (`id`, `orderDetailId`, `orderDetailItem`, `orderDetailItemQuantity`, `orderDetailItemPrice`, `orderDetailMerchant`) VALUES
(1, 'd52a0843-2b2e-4ba8-8f59-ce97739eedd4', 'iPhone', 1, 14000000, 'iBox'),
(2, 'd52a0843-2b2e-4ba8-8f59-ce97739eedd4', 'Charger WiFi Universel', 1, 1500000, 'Erafone'),
(3, 'f52a0843-2b2e-4ba8-8f59-ce97739eedd3', 'PS 2', 1, 2000000, 'Playstation');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order_item_detail`
--
ALTER TABLE `order_item_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `order_item_detail`
--
ALTER TABLE `order_item_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
