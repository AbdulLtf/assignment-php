@extends('layout/main')
@section('title','Halaman Data')
    
@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
    <h3>Form Tambah Data Order Item</h3>
    </div>
    <div>
    <form method="post" action="/data">
        @csrf
        <div class="form-group">
          <label for="orderDetailId">Order Detail ID</label>
        <input name="orderDetailId" type="text" class="form-control @error('orderDetailId') is-invalid @enderror" id="orderDetailId" placeholder="Masukan Order Detail ID" value="{{old('orderDetailId')}}">
        </div>
        <div class="form-group">
          <label for="orderDetailItem">Detail item</label>
          <input name="orderDetailItem" type="text" class="form-control @error('orderDetailItem') is-invalid @enderror" id="orderDetailItem" placeholder="Masukan Detail Item" value="{{old('orderDetailItem')}}">
        </div>
        <div class="form-group">
          <label for="orderDetailItemQuantity">Item Quantity</label>
          <input name="orderDetailItemQuantity" type="text" class="form-control @error('orderDetailItemQuantity') is-invalid @enderror" id="orderDetailItemQuantity" placeholder="Masukan Order Item Quantity" value="{{old('orderDetailItemQuantity')}}">
        </div>
        <div class="form-group">
          <label for="orderDetailItemPrice">Item Price</label>
          <input name="orderDetailItemPrice" type="text" class="form-control @error('orderDetailItemPrice') is-invalid @enderror" id="orderDetailItemPrice" placeholder="Masukan Deskripsi" value="{{old('orderDetailItemPrice')}}">
        </div>
        <div class="form-group">
          <label for="orderDetailMerchant">Merchant</label>
          <input name="orderDetailMerchant" type="text" class="form-control @error('orderDetailMerchant') is-invalid @enderror" id="orderDetailMerchant" placeholder="Masukan Deskripsi" value="{{old('orderDetailMerchant')}}">
        </div>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </form>
    </div>
</div>
@endsection

