@extends('layout/main')
@section('title','Halaman Data')
    
@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
    <h3>Data Order</h3>
      <a href="data/create" class="btn btn-primary">Tambah Data</a>
      @if(session('Status'))
        <div class="alert alert-success">
            {{session('Status')}}
        </div>
      @endif
    </div>
    <div>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Order Id</th>
            <th scope="col">Invoice</th>
            <th scope="col">Order Name</th>
            <th scope="col">Order Description</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $datas)
              
          <tr>
          <th scope="row">{{$loop->iteration}}</th>
            <td>{{$datas->orderId}}</td>
            <td>{{$datas->invoceNumber}}</td>
            <td>{{$datas->orderName}}</td>
            <td>{{$datas->orderDescription}}</td>
            <td>
            <a href="/data/{{$datas->id}}" class="badge badge-info">detail</a>
              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

