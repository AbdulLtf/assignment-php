@extends('layout/main')
@section('title','Halaman Data')
    
@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
    <h3>Form Tambah Data Order Item</h3>
    </div>
    <div>
    <form method="post" action="/data">
        @csrf
        <div class="form-group">
          <label for="orderId">Order ID</label>
        <input name="orderId" type="text" class="form-control @error('orderId') is-invalid @enderror" id="orderId" placeholder="Masukan Order ID" value="{{old('orderId')}}">
        </div>
        <div class="form-group">
          <label for="invoceNumber">Invoice</label>
          <input name="invoceNumber" type="text" class="form-control @error('invoceNumber') is-invalid @enderror" id="invoceNumber" placeholder="Masukan Invoice" value="{{old('lastname')}}">
        </div>
        <div class="form-group">
          <label for="orderName">Nama Order</label>
          <input name="orderName" type="text" class="form-control @error('orderName') is-invalid @enderror" id="orderName" placeholder="Masukan Order Name" value="{{old('orderName')}}">
        </div>
        <div class="form-group">
          <label for="orderDescription">Deskripsi</label>
          <input name="orderDescription" type="text" class="form-control @error('orderDescription') is-invalid @enderror" id="orderDescription" placeholder="Masukan Deskripsi" value="{{old('orderDescription')}}">
        </div>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </form>
    </div>
</div>
@endsection

