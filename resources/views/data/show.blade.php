@extends('layout/main')
@section('title','Detail Data')
    
@section('container')
<div class="container">
<div>Detail Order Item</div>
<a href="dataDetail/create" class="btn btn-primary">Tambah Data</a>
@foreach ($data as $data)
  <div class="card" style="width: 18rem; display:inline-flex">
    <ul class="list-group list-group-flush d-inline">
    <li class="list-group-item">Item ke- {{$loop->iteration}}</li>
    <li class="list-group-item"><span>Order Id: </span>{{$data->orderDetailId}}</li>
      <li class="list-group-item"><span>Item Detail: </span>{{$data->orderDetailItem}}</li>
      <li class="list-group-item"><span>Quantity: </span>{{$data->orderDetailItemQuantity}}</li>
      <li class="list-group-item"><span>Price: </span>{{$data->orderDetailItemPrice}}</li>
      <li class="list-group-item"><span>Merchant: </span>{{$data->orderDetailMerchant}}</li>
    </ul>
  </div>
  @endforeach
  <a href="/data" class="d-block">Kembali</a>
  </div>
</div>
@endsection