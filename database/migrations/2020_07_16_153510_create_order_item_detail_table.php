<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('orderDetailId');
            $table->string('orderDetailItem');
            $table->integer('orderDetailItemQuantity');
            $table->integer('orderDetailItemPrice');
            $table->string('orderDetailMerchant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item_detail');
    }
}
