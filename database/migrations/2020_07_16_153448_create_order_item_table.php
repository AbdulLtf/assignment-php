<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('orderId');
            $table->string('invoceNumber');
            $table->string('orderName');
            $table->string('orderDescription');
            $table->string('createdBy');
            $table->dateTimeTz('createdDate');
            $table->string('modifiedBy');
            $table->dateTimeTz('ModifiedDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
