<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');

Route::get('/data', 'DataController@index');
Route::get('/data/create', 'DataController@create');
Route::get('/data/dataDetail/create', 'DataDetailController@create');

Route::get('/data/{data}', 'DataController@show');

Route::post('/data', 'DataController@store');
Route::post('/data', 'DataDetailController@store');
